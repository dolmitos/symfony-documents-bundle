"use strict";

class DocumentList {

    constructor(config) {
        this.selector = config.selector || undefined;
        this.containerSelector = config.containerSelector || undefined;
        this.dropzoneSelector = config.dropzoneSelector || undefined;
        this.dataTableName = config.dataTableName || undefined;
        this.dataTableOptions = config.dataTableOptions || undefined;

        this.postUrl = config.postUrl || undefined;
        this.deleteUrl = config.deleteUrl || undefined;

        this.fileInputName = 'document_list_form[file][file]';

        this.dataTable = undefined;
        this.dropZone = undefined;

        this.initialize();
    }

    initialize() {
        this.initializeFormatters(this.dataTableOptions);

        this.initializeDataTable();

        this.initializeDropZone();
    }

    initializeFormatters(options) {
        let self = this;

        self.formatters = {};

        if (options.columns) {
            self.initializeColumnFormatters(options.columns);
        }
    }

    initializeColumnFormatters(columns) {
        let self = this;

        columns.forEach(function(item, index) {
            if (item.columns !== undefined) {
                self.initializeColumnFormatters(item.columns);
                return;
            }

            let formatter = item.formatter;

            if (item.field === 'fileData.originalName') {
                self.formatters[item.field] = function(element, cell, formatterParams, onRendered) {
                    element.querySelector('a').setAttribute('href', element.querySelector('a').getAttribute('href').replace('__DOCUMENT_ID__', cell.getRow().getData().id));

                    return element;
                };
            }
            if (item.field === 'actions') {
                self.formatters[item.field] = function(element, cell, formatterParams, onRendered) {
                    let attachFnc = self.attachDeleteClickEvent(cell.getRow());
                    attachFnc();
                    return element;
                };
            }

            if (undefined !== self.formatters[item.field]) {
                item.formatter = function(cell, formatterParams, onRendered) {
                    let fakeOnRendered = function(){};
                    let element;
                    if (formatter) element = formatter.call(this, cell, formatterParams, fakeOnRendered);
                    element = self.formatters[item.field].call(this, element, cell, formatterParams, onRendered);
                    return element;
                }
            }
        });
    }

    initializeDataTable() {
        if (document.getElementById(this.dataTableName + '_placeholder_template_source')) {
            DataTables.tabulator.placeholders[this.selector] = [];
            DataTables.tabulator.placeholders[this.selector]['empty'] = document.getElementById(this.dataTableName + '_placeholder_template_source').querySelector('.tabulator-placeholder.empty');
            DataTables.tabulator.placeholders[this.selector]['loading'] = document.getElementById(this.dataTableName + '_placeholder_template_source').querySelector('.tabulator-placeholder.loading');
        }

        // Initialize heightAdjuster before the Tabulator is loaded to be able to account for data being set directly at init.
        DataTables.tabulator.heightAdjuster.initializeConfig(null, this.dataTableOptions, this.selector);

        let dataTable = this.dataTable = new DataTable({
            selector: this.selector,
            name: this.dataTableName,
            type: DataTable.typeTabulator(),
            options: this.dataTableOptions
        });

        DataTables.add(dataTable);
    }

    initializeDropZone() {
        let self = this;

        let Dropzone = require('dropzone');
        Dropzone.autoDiscover = false;

        let dropZone = self.dropZone = new Dropzone(self.dropzoneSelector, {
            url: self.postUrl,
            paramName: self.fileInputName,
            withCredentials: true,
            autoProcessQueue: true,
        });

        dropZone.on("success", function(file, response) {
            window.setTimeout(function(){
                dropZone.removeFile(file);
            },200);

            if (response.document) {
                let document = response.document;
                document.edit = false;
                self.dataTable.addRow(document, true);
                self.dataTable.getTabulator().redraw();
            }
        });

        dropZone.on("queuecomplete", function() {
            window.setTimeout(function(){
                dropZone.removeAllFiles();
            },200);
        });


    }

    attachDeleteClickEvent(rowComponent) {
        let self = this;

        let returnFunc = function() {
            clearInterval(returnFunc.timeout);
            returnFunc.timeout = setInterval(function() {
                if (rowComponent._row.initialized) {
                    clearInterval(returnFunc.timeout);

                    rowComponent.getCell('actions').getElement().querySelector('.file-remove').addEventListener('click', function() {
                        self.deleteDocument(rowComponent, rowComponent.getData().id);
                    });
                }
            },0);
        };
        return returnFunc;
    }

    deleteDocument(rowComponent, documentId) {
        let url = (' ' + this.deleteUrl).slice(1).replace('__DOCUMENT_ID__', documentId);

        let headers = new Headers({
            'X-Requested-With': 'XMLHttpRequest',
        });

        let promise = new Promise(function(resolve, reject) {
            fetch(url, {method: 'DELETE', headers: headers})
                .then(function (response) {
                    if (response.status === 204) {
                        resolve();
                    } else {
                        reject();
                    }
                })
                .catch(err => {
                    reject(err);
                });
        });

        promise.then(function () {
            rowComponent.delete();
        }, function (error) {});
    }

}


module.exports = DocumentList;
