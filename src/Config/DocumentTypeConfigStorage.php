<?php

namespace DolmIT\DocumentsBundle\Config;

class DocumentTypeConfigStorage implements \Iterator
{
    /**
     * @var array|null
     */
    protected $config;

    public function __construct(?array $config)
    {
        $this->processConfig($config);
    }

    private function processConfig(?array $config)
    {
        if ($config && isset($config['types']) && \count($config['types']) > 0) {
            foreach ($config['types'] as $key => $attributes) {
                $this->config[$key] = $attributes['class'];
            }
        }
    }

    public function getClassByType(string $type)
    {
        return $this->config[$type];
    }

    public function getTypeByClass(string $class)
    {
        return array_keys(array_filter($this->config, function ($cl) use ($class) {
            return $cl == $class;
        }))[0];
    }

    /**
     * Return the current element.
     *
     * @see  https://php.net/manual/en/iterator.current.php
     *
     * @return mixed can return any type
     *
     * @since 5.0.0
     */
    public function current()
    {
        $var = current($this->config);

        return $var;
    }

    /**
     * Move forward to next element.
     *
     * @see  https://php.net/manual/en/iterator.next.php
     * @since 5.0.0
     */
    public function next()
    {
        $var = next($this->config);

        return $var;
    }

    /**
     * Return the key of the current element.
     *
     * @see  https://php.net/manual/en/iterator.key.php
     *
     * @return mixed scalar on success, or null on failure
     *
     * @since 5.0.0
     */
    public function key()
    {
        $var = key($this->config);

        return $var;
    }

    /**
     * Checks if current position is valid.
     *
     * @see  https://php.net/manual/en/iterator.valid.php
     *
     * @return bool The return value will be casted to boolean and then evaluated.
     *              Returns true on success or false on failure.
     *
     * @since 5.0.0
     */
    public function valid()
    {
        $key = key($this->config);
        $var = (null !== $key && false !== $key);

        return $var;
    }

    /**
     * Rewind the Iterator to the first element.
     *
     * @see  https://php.net/manual/en/iterator.rewind.php
     * @since 5.0.0
     */
    public function rewind()
    {
        reset($this->config);
    }
}
