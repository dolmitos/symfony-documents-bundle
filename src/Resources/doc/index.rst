Getting Started With DolmITDocumentsBundle
==================================


Prerequisites
-------------

This version of the bundle requires Symfony 4+.


Installation
------------

1. Download dolmitos/symfony-documents-bundle using composer
2. Enable the Bundle

Step 1: Download DolmITDocumentsBundle using composer
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Require the bundle with composer:

.. code-block:: bash

    $ composer require dolmitos/symfony-documents-bundle

Composer will install the bundle to your project's ``vendor/dolmitos/symfony-documents-bundle`` directory.

Step 2: Enable the bundle
~~~~~~~~~~~~~~~~~~~~~~~~~

Enable the bundle in the kernel::

    <?php
    // app/AppKernel.php

    public function registerBundles()
    {
        $bundles = array(
            // ...
            new DolmIT\DocumentsBundle\DolmITDocumentsBundle(),
            // ...
        );
    }
    ?>

Add the twig theme to your project in config/packages/twig.yaml::

    twig:
        form_themes:
            - '@DolmITDocuments/Form/fields.html.twig'


Create a configuration file in configs/packages/dolm_it_documents.yaml or add to services.yaml the following::

    dolm_it_documents:
        types:
            alphanumeric_snake_case_format_name: App\Full\ClassName


Finally, register this bundle's routes by add the following to your projects' routing file::

        # ./config/routing.yml
        dolm_it_documents:
            resource: "@DolmITDocumentsBundle/Resources/config/routing.yaml"

Step 3: Satisfy requirements
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

1. Must include symfony-datatables-bundle datatables.js in the page with the documents block or globally

Step 4: Usage
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

1. Create entities as you see fit, but they must implement DocumentInterface
2. Create a configuration for VichUploaderBundle and attach it to your Entity as per the instructions noted in the bundle


Step 5: Custom table structure (Optional)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

You may need to build your own datatable columns, as You control the entity data.


1. When creating your own DataTable instance, you must extend the DocumentDataTable provided by the bundle, but only if you wish to use the default functionality.
2. The custom DataTable should call parent::buildDataTable($options); at the end, if you wish to add the default fields
2.1. If a field already exists, the DocumentDataTable will not overwrite it and will simply skip it.


Step 6: Custom Controller (Optional)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In some cases you may want to override the controller. For example, when you need to use options for the DataTable generation.
In such a case, you can create your own controller and simply pass the routes the DocumentManager to create the DocumentList.

To override the routes, simply send the following structure to the DocumentManagerInterface::getDocumentList()::

    // Override only the ones you plan to use, if you omit one, the default will be used.

    [
        'index' => 'dolmit_documents_bundle_index',
        'post' => 'dolmit_documents_bundle_post',
        'delete' => 'dolmit_documents_bundle_delete',
    ]


Step 7: Create a DocumentList object and output the table
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

1. Inject the DocumentManager in the controller and use it::

    $documentList = $documentManager->getDocumentList(QuoteDocument::class, $quote->getId());

2. Output the table in the view::

    {{ render_document_list(documentList) }}

2.1. OR output the javascript and html separately::

    {{ render_document_list_datatable(documentList) }}

    <script>
        {{ render_document_list_javascript(documentList)|raw }}
    </script>

