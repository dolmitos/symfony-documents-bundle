<?php

namespace DolmIT\DocumentsBundle\DataTable;

use DolmIT\DataTablesBundle\DataTable\AbstractDataTable;
use DolmIT\DataTablesBundle\DataTable\Column\Column;
use DolmIT\DataTablesBundle\DataTable\Column\ColumnInterface;
use DolmIT\DataTablesBundle\DataTable\DataTableInterface;
use DolmIT\DocumentsBundle\Document\DocumentListInterface;
use DolmIT\DocumentsBundle\Form\DocumentViewFormType;

class DocumentDataTable extends AbstractDataTable
{
    protected $translationDomain = 'documents';

    protected $type = DataTableInterface::TABLE_TYPE_TABULATOR;

    /**
     * @var DocumentListInterface
     */
    protected $documentList;

    /**
     * Builds the datatable.
     *
     * @param array $options
     *
     * @throws \Exception
     */
    public function buildDataTable(array $options = [])
    {
        $formFields = [];

        $this->setDocumentList($options['documentList']);

        /*
         * NB! If you need to override any of these, then it's best to completely overwrite the class and not call the parent,
         * as there are only two options:
         * 1. Call the parent::buildDataTable first and add all the fields you may not want
         * 2. Call the parent::buildDataTable second and override all options you already set that exist in the parent
         */
        $this->getOptions()->set([
            'layout' => 'fitColumns',
            'table_classes' => 'table-striped table-bordered',

            'nested_field_separator' => '.',
            'placeholder' => 'No data set',

            'height' => '200',
            'dynamic_height_rows' => true,
            'dynamic_height_row_count' => 5,
            'dynamic_height_min' => 200,

            // Remove the "Loading" overlay
            'ajax_loader' => false,

            'pagination_add_row' => 'table',

            'ajax_sorting' => false,
            'ajax_u_r_l' => $this->getDocumentList()->getIndexPath(),
        ]);

        $existingColumnIdentifiers = array_map(function (ColumnInterface $column) {
            return $column->getIdentifier();
        }, $this->getColumnBuilder()->getColumns());

        if (!\in_array('fileData.originalName', $existingColumnIdentifiers)) {
            $this->getColumnBuilder()->add('fileData.originalName', Column::class, [
                'title' => 'documents.fileData.originalName',
            ]);
        }
        if (!\in_array('fileData.size', $existingColumnIdentifiers)) {
            $this->getColumnBuilder()->add('fileData.size', Column::class, [
                'title' => 'documents.fileData.size',
                'headerSort' => false,
                'width' => '63',
            ]);
        }

        if (!\in_array('actions', $existingColumnIdentifiers)) {
            $this->getColumnBuilder()->add('actions', Column::class, [
                'title' => 'documents.actions',
                'headerSort' => false,
                'width' => '40',
            ]);
        }

        if ($this->getViewFormBuilder()->getFormBuilder()) {
            foreach ($formFields as $identifier => $field) {
                if (!$this->getViewFormBuilder()->getFormBuilder()->get($identifier)) {
                    $this->getViewFormBuilder()->getFormBuilder()->add($identifier, $field['class'], $field['options'] ?? []);
                }
            }
        } else {
            $this->getViewFormBuilder()->create(
                DocumentViewFormType::class,
                null,
                [
                    'document_list' => $this->getDocumentList(),
                    'translator' => $this->getTranslator(),
                    'entry_options' => [// We need to duplicate these as entry_options is what the prototype will be able to use
                            'document_list' => $this->getDocumentList(),
                            'translator' => $this->getTranslator(),
                        ],
                ]
            );
        }
    }

    /**
     * @return DocumentListInterface
     */
    public function getDocumentList(): DocumentListInterface
    {
        return $this->documentList;
    }

    /**
     * @param DocumentListInterface $documentList
     *
     * @return $this
     */
    public function setDocumentList(DocumentListInterface $documentList): self
    {
        $this->documentList = $documentList;

        return $this;
    }
}
