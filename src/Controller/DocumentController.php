<?php

namespace DolmIT\DocumentsBundle\Controller;

use DolmIT\DataTablesBundle\Response\DataTableResponse;
use DolmIT\DocumentsBundle\Config\DocumentTypeConfigStorage;
use DolmIT\DocumentsBundle\Document\DocumentManagerInterface;
use DolmIT\DocumentsBundle\Form\DocumentListFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Vich\UploaderBundle\Handler\DownloadHandler;

class DocumentController extends AbstractController
{
    /**
     * @var DocumentManagerInterface
     */
    protected $documentManager;

    /**
     * @var DocumentTypeConfigStorage
     */
    protected $documentTypeConfigStorage;

    /**
     * @var NormalizerInterface
     */
    protected $normalizer;

    /**
     * @var DownloadHandler
     */
    protected $downloadHandler;

    public function __construct(
        DocumentManagerInterface $documentManager,
        DocumentTypeConfigStorage $documentTypeConfigStorage,
        NormalizerInterface $normalizer,
        DownloadHandler $downloadHandler
    ) {
        $this->documentManager = $documentManager;
        $this->documentTypeConfigStorage = $documentTypeConfigStorage;
        $this->normalizer = $normalizer;
        $this->downloadHandler = $downloadHandler;
    }

    /**
     * @param Request     $request
     * @param string      $type
     * @param string|null $identifier
     *
     * @return JsonResponse
     */
    public function index(Request $request, string $type, ?string $identifier = null)
    {
        $documentClass = $this->getDocumentTypeConfigStorage()->getClassByType($type);
        $documentDataTable = \constant($documentClass.'::DATATABLE_CLASS') ?? null;

        $documentList = $this->getDocumentManager()->getDocumentList($documentClass, $identifier, $documentDataTable);

        $dataTableResponse = new DataTableResponse($documentList->getDataTable());

        return $dataTableResponse->getResponse();
    }

    /**
     * @param Request     $request
     * @param string      $type
     * @param string|null $identifier
     *
     * @return JsonResponse
     *
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function post(Request $request, string $type, ?string $identifier = null)
    {
        $documentClass = $this->getDocumentTypeConfigStorage()->getClassByType($type);

        $form = $this->createForm(DocumentListFormType::class);
        $form->handleRequest($request);

        $document = $this->getDocumentManager()->saveDocument($documentClass, $form, $identifier);

        return new JsonResponse(['document' => $this->getNormalizer()->normalize($document, 'array')], ($document ? Response::HTTP_CREATED : Response::HTTP_INTERNAL_SERVER_ERROR));
    }

    /**
     * @param Request $request
     * @param string  $type
     * @param string  $document
     *
     * @return JsonResponse
     */
    public function delete(Request $request, string $type, string $document)
    {
        $documentClass = $this->getDocumentTypeConfigStorage()->getClassByType($type);

        $this->getDocumentManager()->deleteDocument($documentClass, $document);

        return new JsonResponse([], Response::HTTP_NO_CONTENT);
    }

    /**
     * @param Request $request
     * @param string  $type
     * @param string  $document
     *
     * @return Response
     */
    public function download(Request $request, string $type, string $document): Response
    {
        $documentClass = $this->getDocumentTypeConfigStorage()->getClassByType($type);
        $document = $this->getDocumentManager()->getDocument($documentClass, $document);
        if (!$document) {
            return new Response(null, Response::HTTP_NOT_FOUND);
        }

        return $this->getDownloadHandler()->downloadObject($document, 'file', null, true, true);
    }

    /**
     * @return DocumentManagerInterface
     */
    public function getDocumentManager(): DocumentManagerInterface
    {
        return $this->documentManager;
    }

    /**
     * @return DocumentTypeConfigStorage
     */
    public function getDocumentTypeConfigStorage(): DocumentTypeConfigStorage
    {
        return $this->documentTypeConfigStorage;
    }

    /**
     * @return NormalizerInterface
     */
    public function getNormalizer(): NormalizerInterface
    {
        return $this->normalizer;
    }

    /**
     * @return DownloadHandler
     */
    public function getDownloadHandler(): DownloadHandler
    {
        return $this->downloadHandler;
    }
}
