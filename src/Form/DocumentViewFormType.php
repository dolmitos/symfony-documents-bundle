<?php

namespace DolmIT\DocumentsBundle\Form;

use DolmIT\DataTablesBundle\DataTable\Form\PrototypeFormType;
use DolmIT\DocumentsBundle\Document\DocumentListInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

class DocumentViewFormType extends PrototypeFormType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('actions', DolmitosDocumentsBundleIType::class, [
                'attr' => [
                    'class' => 'fa fa-trash file-remove input-xs',
                    'title' => $options['translator']->trans('documents.remove', [], $options['translation_domain'] ?? 'documents'),
                ],
            ])
            ->add('fileData', DocumentViewFileDataFormType::class, [
                'document_list' => $options['document_list'],
            ])
        ;

        parent::buildForm($builder, $options);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'document_list' => null,
            'translator' => null,
            'entry_options' => [],
        ]);

        $resolver->addAllowedTypes('document_list', ['null', DocumentListInterface::class]);
        $resolver->addAllowedTypes('translator', ['null', TranslatorInterface::class]);

        parent::configureOptions($resolver);
    }

    public function getBlockPrefix()
    {
        return 'document_view';
    }
}
