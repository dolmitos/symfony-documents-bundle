<?php

namespace DolmIT\DocumentsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DolmitosDocumentsBundleHrefType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([
            'required' => false,
            'href' => '',
            'value' => '',
        ]);

        $resolver->setAllowedTypes('href', ['string', 'null']);
        $resolver->setAllowedTypes('value', ['string', 'null']);
    }

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);

        $view->vars['attr']['href'] = $options['href'];
        $view->vars['value'] = $options['value'];
    }

    public function getParent()
    {
        return TextType::class;
    }
}
