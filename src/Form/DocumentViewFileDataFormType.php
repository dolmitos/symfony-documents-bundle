<?php

namespace DolmIT\DocumentsBundle\Form;

use DolmIT\DocumentsBundle\Document\DocumentListInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DocumentViewFileDataFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('originalName', DolmitosDocumentsBundleHrefType::class, [
            'required' => false,
            'href' => $options['document_list']->getDownloadPath(),
        ]);

        parent::buildForm($builder, $options);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'document_list' => null,
        ]);

        $resolver->addAllowedTypes('document_list', ['null', DocumentListInterface::class]);

        parent::configureOptions($resolver);
    }
}
