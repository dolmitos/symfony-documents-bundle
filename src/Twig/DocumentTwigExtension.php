<?php

namespace DolmIT\DocumentsBundle\Twig;

use DolmIT\DocumentsBundle\Document\DocumentListInterface;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessor;
use Twig_Environment;
use Twig_Extension;
use Twig_SimpleFunction;

/**
 * Class DataTableTwigExtension.
 */
class DocumentTwigExtension extends Twig_Extension
{
    /**
     * The PropertyAccessor.
     *
     * @var PropertyAccessor
     */
    protected $accessor;

    /**
     * DocumentTwigExtension constructor.
     */
    public function __construct()
    {
        $this->accessor = PropertyAccess::createPropertyAccessor();
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'dolmit_documents_twig_extension';
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return [
            new Twig_SimpleFunction(
                'render_document_list',
                [$this, 'renderDocumentList'],
                ['is_safe' => ['html'], 'needs_environment' => true]
            ),
            new Twig_SimpleFunction(
                'render_document_list_datatable',
                [$this, 'renderDocumentListDataTable'],
                ['is_safe' => ['html'], 'needs_environment' => true]
            ),
            new Twig_SimpleFunction(
                'render_document_list_javascript',
                [$this, 'renderDocumentListJavaScript'],
                ['is_safe' => ['html'], 'needs_environment' => true]
            ),
        ];
    }

    public function renderDocumentList(Twig_Environment $twig, DocumentListInterface $documentList)
    {
        return $twig->render(
            '@DolmITDocuments/DocumentList/document_list.html.twig',
            [
                'documentList' => $documentList,
            ]
        );
    }

    public function renderDocumentListDataTable(Twig_Environment $twig, DocumentListInterface $documentList)
    {
        return $twig->render(
            '@DolmITDocuments/DocumentList/document_datatable.html.twig',
            [
                'documentList' => $documentList,
            ]
        );
    }

    public function renderDocumentListJavaScript(Twig_Environment $twig, DocumentListInterface $documentList)
    {
        return $twig->render(
            '@DolmITDocuments/DocumentList/document_list.js.twig',
            [
                'documentList' => $documentList,
            ]
        );
    }
}
