<?php

namespace DolmIT\DocumentsBundle\Document;

use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\ORM\Mapping\ManyToOne;
use DolmIT\DataTablesBundle\DataTable\Exception\DocumentRelationManagerException;

class DocumentRelationManager implements DocumentRelationManagerInterface
{
    /**
     * @var AnnotationReader
     */
    protected $annotationReader;

    /**
     * @param string $documentClass
     * @param null   $documentRelationIdentifier
     *
     * @return DocumentRelation|null
     *
     * @throws DocumentRelationManagerException
     * @throws \ReflectionException
     */
    public function getDocumentRelation(string $documentClass, $documentRelationIdentifier = null): ?DocumentRelation
    {
        $documentRelationObject = null;
        if ($documentRelationIdentifier) {
            $relationClass = $relationPropertyName = null;
            $propertyAnnotation = $this->getAnnotationReader()->getPropertyAnnotation(new \ReflectionProperty($documentClass, \constant($documentClass.'::RELATION_FIELD')), 'Doctrine\ORM\Mapping\ManyToOne');
            if ($propertyAnnotation && $propertyAnnotation instanceof ManyToOne) {
                $relationClass = $propertyAnnotation->targetEntity;
                $relationPropertyName = $propertyAnnotation->inversedBy;
            }
            if (!$relationClass) {
                throw new DocumentRelationManagerException('DocumentRelationManager::getDocumentRelation(): Could not determine the class of the relation, is the inverse side of the relation missing?');
            }
            $documentRelationObject = new DocumentRelation($relationClass, $this->getDocumentRelationIdentifier($documentRelationIdentifier), $relationPropertyName);
        }

        return $documentRelationObject;
    }

    /**
     * @param null $documentRelation
     *
     * @return string|null
     *
     * @throws DocumentRelationManagerException
     */
    protected function getDocumentRelationIdentifier($documentRelation = null)
    {
        if ($documentRelation) {
            if ($documentRelation && \is_object($documentRelation)) {
                try {
                    $documentRelation = (string) $documentRelation;
                } catch (\Exception $e) {
                    throw new DocumentRelationManagerException('DocumentRelationManager::getDocumentRelationIdentifier(): '.\get_class($documentRelation).' expected to be an instance of an object that contains the __toString method.');
                }
            } else {
                $documentRelation = (string) $documentRelation;
            }

            return $documentRelation;
        }

        return null;
    }

    /**
     * @return AnnotationReader
     */
    public function getAnnotationReader(): AnnotationReader
    {
        return $this->annotationReader;
    }

    /**
     * @param AnnotationReader $annotationReader
     *
     * @return DocumentListFactory
     */
    public function setAnnotationReader(AnnotationReader $annotationReader): self
    {
        $this->annotationReader = $annotationReader;

        return $this;
    }
}
