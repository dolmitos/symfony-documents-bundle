<?php

namespace DolmIT\DataTablesBundle\DataTable\Exception;

class DocumentListFactoryException extends \Exception
{
}
