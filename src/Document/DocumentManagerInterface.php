<?php

namespace DolmIT\DocumentsBundle\Document;

use Symfony\Component\Form\FormInterface;

interface DocumentManagerInterface
{
    /**
     * Supply a document identifier, either the class or the identifier set in the configuration file
     * and a relation key to it's parent entity, if a relation should exist.
     *
     * @param string $documentClass
     * @param null   $documentRelation
     * @param null   $documentDataTable
     * @param array  $documentDataTableOptions - The argument will be attached to the generated DataTable, but cannot be used to generate the return data in the DocumentController
     *
     * @return DocumentListInterface
     */
    public function getDocumentList(
        string $documentClass,
        $documentRelation = null,
        $documentDataTable = null,
        $documentDataTableOptions = []): DocumentListInterface;

    /**
     * @param string        $documentClass
     * @param FormInterface $form
     * @param null          $documentRelation
     *
     * @return DocumentInterface|null
     */
    public function saveDocument(string $documentClass, FormInterface $form, $documentRelation = null): ?DocumentInterface;

    /**
     * @param string $documentClass
     * @param string $documentIdentifier
     *
     * @return bool
     */
    public function deleteDocument(string $documentClass, string $documentIdentifier): bool;

    /**
     * @param string $documentClass
     * @param string $documentIdentifier
     *
     * @return DocumentInterface|null
     */
    public function getDocument(string $documentClass, string $documentIdentifier): ?DocumentInterface;
}
