<?php

namespace DolmIT\DocumentsBundle\Document;

class DocumentRelation
{
    protected $class;

    protected $identifier;

    protected $propertyName;

    public function __construct(
        string $class,
        string $identifier,
        string $propertyName
    ) {
        $this->class = $class;
        $this->identifier = $identifier;
        $this->propertyName = $propertyName;
    }

    /**
     * @return string
     */
    public function getClass(): string
    {
        return $this->class;
    }

    /**
     * @return string
     */
    public function getIdentifier(): string
    {
        return $this->identifier;
    }

    /**
     * @return string
     */
    public function getPropertyName(): string
    {
        return $this->propertyName;
    }
}
