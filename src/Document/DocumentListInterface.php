<?php

namespace DolmIT\DocumentsBundle\Document;

use DolmIT\DataTablesBundle\DataTable\DataTableInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Routing\RouterInterface;

interface DocumentListInterface
{
    /**
     * @return RouterInterface
     */
    public function getRouter(): RouterInterface;

    /**
     * @return DataTableInterface
     */
    public function getDataTable(): DataTableInterface;

    /**
     * @return FormInterface
     */
    public function getForm(): FormInterface;

    /**
     * @return FormView
     */
    public function getFormView(): FormView;

    /**
     * Return the className of the document this list is based off of.
     *
     * @return string
     */
    public function getDocumentClass(): string;

    /**
     * Return the relation to the parent element of the document, if one exists.
     *
     * @return null|mixed
     */
    public function getDocumentRelation();

    /**
     * Return the routes used for the DocumentList.
     *
     * @return array
     */
    public function getRoutes(): array;

    /**
     * @return string
     */
    public function getIndexPath(): string;

    /**
     * @return string
     */
    public function getPostPath(): string;

    /**
     * @return string
     */
    public function getDeletePath(): string;
}
