<?php

namespace DolmIT\DocumentsBundle\Document;

interface DocumentListFactoryInterface
{
    /**
     * Create a DocumentList.
     *
     * @param string      $documentClass
     * @param string|null $documentRelation
     * @param string|null $documentDataTable
     * @param array       $documentDataTableOptions
     * @param array|null  $documentListRoutes
     *
     * @return DocumentListInterface
     */
    public function create(string $documentClass, ?string $documentRelation = null, ?string $documentDataTable = null, ?array $documentDataTableOptions = [], ?array $documentListRoutes = []): DocumentListInterface;
}
