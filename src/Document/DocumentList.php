<?php

namespace DolmIT\DocumentsBundle\Document;

use DolmIT\DataTablesBundle\DataTable\DataTableInterface;
use DolmIT\DocumentsBundle\DataTable\DocumentDataTable;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Routing\RouterInterface;

class DocumentList implements DocumentListInterface
{
    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var DocumentDataTable
     */
    private $dataTable;

    /**
     * @var FormInterface
     */
    private $form;

    /**
     * @var FormView
     */
    private $formView;

    /**
     * Class name for the type of Document Entity the DocumentList contains.
     *
     * @var string
     */
    private $documentClass;

    /**
     * Identifying string to help match the route with the document type.
     *
     * @var string
     */
    private $documentConfigIdentifier;

    /**
     * Relation object that is matched to the document entity parent entity, if one exists.
     *
     * @var DocumentRelation|null
     */
    private $documentRelation = null;

    private $routes = [
        'index' => 'dolmit_documents_bundle_index',
        'post' => 'dolmit_documents_bundle_post',
        'delete' => 'dolmit_documents_bundle_delete',
        'download' => 'dolmit_documents_bundle_download',
    ];

    public function __construct(
        RouterInterface $router,
        string $documentClass,
        string $documentConfigIdentifier,
        ?DocumentRelation $documentRelation = null,
        ?array $routeOverrides = []
    ) {
        $this->router = $router;
        $this->documentClass = $documentClass;
        $this->documentConfigIdentifier = $documentConfigIdentifier;
        $this->documentRelation = $documentRelation;

        if ($routeOverrides) {
            $this->routes = array_merge($this->routes, $routeOverrides);
        }
    }

    /**
     * @return string
     */
    public function getIndexPath(): string
    {
        return $this->getRouter()->generate($this->getRoutes()['index'], ['type' => $this->getDocumentConfigIdentifier(), 'identifier' => $this->getDocumentRelation()->getIdentifier()]);
    }

    /**
     * @return string
     */
    public function getPostPath(): string
    {
        return $this->getRouter()->generate($this->getRoutes()['post'], ['type' => $this->getDocumentConfigIdentifier(), 'identifier' => $this->getDocumentRelation()->getIdentifier()]);
    }

    /**
     * @return string
     */
    public function getDeletePath(): string
    {
        return $this->getRouter()->generate($this->getRoutes()['delete'], ['type' => $this->getDocumentConfigIdentifier(), 'document' => '__DOCUMENT_ID__']);
    }

    /**
     * @return string
     */
    public function getDownloadPath(): string
    {
        return $this->getRouter()->generate($this->getRoutes()['download'], ['type' => $this->getDocumentConfigIdentifier(), 'document' => '__DOCUMENT_ID__']);
    }

    /**
     * @return RouterInterface
     */
    public function getRouter(): RouterInterface
    {
        return $this->router;
    }

    /**
     * @param RouterInterface $router
     *
     * @return DocumentList
     */
    public function setRouter(RouterInterface $router): self
    {
        $this->router = $router;

        return $this;
    }

    /**
     * @return DataTableInterface
     */
    public function getDataTable(): DataTableInterface
    {
        return $this->dataTable;
    }

    /**
     * @param DataTableInterface $dataTable
     *
     * @return DocumentList
     */
    public function setDataTable(DataTableInterface $dataTable): self
    {
        $this->dataTable = $dataTable;

        return $this;
    }

    /**
     * @return FormInterface
     */
    public function getForm(): FormInterface
    {
        return $this->form;
    }

    /**
     * @param FormInterface $form
     *
     * @return $this
     */
    public function setForm(FormInterface $form): self
    {
        $this->form = $form;

        return $this;
    }

    /**
     * @return FormView
     */
    public function getFormView(): FormView
    {
        if (!$this->formView) {
            $this->formView = $this->form->createView();
        }

        return $this->formView;
    }

    /**
     * @param FormView $formView
     *
     * @return DocumentList
     */
    public function setFormView(FormView $formView): self
    {
        $this->formView = $formView;

        return $this;
    }

    /**
     * @return string
     */
    public function getDocumentClass(): string
    {
        return $this->documentClass;
    }

    /**
     * @param string $documentClass
     *
     * @return DocumentList
     */
    public function setDocumentClass(string $documentClass): self
    {
        $this->documentClass = $documentClass;

        return $this;
    }

    /**
     * @return DocumentRelation|null
     */
    public function getDocumentRelation(): ?DocumentRelation
    {
        return $this->documentRelation;
    }

    /**
     * @return string
     */
    public function getDocumentConfigIdentifier(): string
    {
        return $this->documentConfigIdentifier;
    }

    /**
     * @param string $documentConfigIdentifier
     *
     * @return DocumentList
     */
    public function setDocumentConfigIdentifier(string $documentConfigIdentifier): self
    {
        $this->documentConfigIdentifier = $documentConfigIdentifier;

        return $this;
    }

    /**
     * @return array
     */
    public function getRoutes(): array
    {
        return $this->routes;
    }

    /**
     * @param array $routes
     *
     * @return $this
     */
    public function setRoutes(array $routes): self
    {
        $this->routes = $routes;

        return $this;
    }
}
