<?php

namespace DolmIT\DocumentsBundle\Document;

use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Vich\UploaderBundle\Entity\File as EmbeddedFile;

interface DocumentInterface
{
    /**
     * Contains the field name for the relation to the document parent or null if there is no parent.
     *
     * This will allow us to obtain the doctrine ORM attributes and be aware of the class and other side property
     * which we can use to obtain all the required data
     */
    const RELATION_FIELD = null;

    /**
     * Contains the full class path of the DataTable that should be used to display the DocumentList that is generated.
     */
    const DATATABLE_CLASS = null;

    /**
     * Must return a string or an object that can be converted to a string through __toString().
     *
     * @return string|object
     */
    public function getId();

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|UploadedFile $file
     *
     * @throws \Exception
     */
    public function setFile(?File $file = null);

    /**
     * @return File|UploadedFile|null
     */
    public function getFile();

    /**
     * @return EmbeddedFile|null
     */
    public function getFileData(): ?EmbeddedFile;

    /**
     * @param EmbeddedFile|null $fileData
     */
    public function setFileData(?EmbeddedFile $fileData);
}
