<?php

namespace DolmIT\DocumentsBundle\Document;

use Doctrine\Common\Persistence\ObjectManager;
use DolmIT\DataTablesBundle\DataTable\DataTableFactory;
use DolmIT\DataTablesBundle\DataTable\Exception\DataTableFactoryException;
use DolmIT\DataTablesBundle\DataTable\Exception\DocumentListFactoryException;
use DolmIT\DocumentsBundle\Config\DocumentTypeConfigStorage;
use DolmIT\DocumentsBundle\DataTable\DocumentDataTable;
use DolmIT\DocumentsBundle\Form\DocumentListFormType;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Twig_Environment;

class DocumentListFactory implements DocumentListFactoryInterface
{
    /**
     * @var ObjectManager
     */
    private $objectManager;

    /**
     * @var NormalizerInterface
     */
    private $normalizer;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * The Twig Environment.
     *
     * @var Twig_Environment
     */
    protected $twig;

    /**
     * @var RouterInterface|null
     */
    protected $router;

    /**
     * @var TokenStorageInterface|null
     */
    protected $tokenStorage;

    /**
     * @var AuthorizationCheckerInterface|null
     */
    protected $authorizationChecker;

    /**
     * @var DataTableFactory
     */
    protected $dataTableFactory;

    /**
     * @var DocumentTypeConfigStorage
     */
    protected $documentTypeConfigStorage;

    /**
     * @var RequestStack
     */
    protected $requestStack;

    /**
     * @var DocumentRelationManagerInterface
     */
    protected $documentRelationManager;

    /**
     * @var FormFactoryInterface
     */
    protected $formFactory;

    /**
     * @param string      $documentClass
     * @param string|null $documentRelation
     * @param string|null $documentDataTable
     * @param array       $documentDataTableOptions
     * @param array|null  $documentListRoutes
     *
     * @return DocumentListInterface
     *
     * @throws DataTableFactoryException
     * @throws DocumentListFactoryException
     * @throws \ReflectionException
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function create(string $documentClass, ?string $documentRelation = null, ?string $documentDataTable = null, ?array $documentDataTableOptions = [], ?array $documentListRoutes = []): DocumentListInterface
    {
        if (!\is_string($documentClass)) {
            $type = \gettype($documentClass);
            throw new DocumentListFactoryException("DocumentListFactory::create(): String expected, $type given");
        }

        if (false === class_exists($documentClass)) {
            throw new DocumentListFactoryException("DocumentListFactory::create(): $documentClass does not exist");
        }

        if (!$documentDataTable) {
            $documentDataTable = \constant($documentClass.'::DATATABLE_CLASS') ?? null;
        }

        if (\in_array(DocumentInterface::class, class_implements($documentClass))) {
            if ($documentDataTable && !is_subclass_of($documentDataTable, DocumentDataTable::class)) {
                throw new DocumentListFactoryException("DocumentListFactory::create(): $documentDataTable does extend ".DocumentDataTable::class.'. Make sure to call parent::buildDataTable at the end as well if you wish to use the default options. ');
            }

            $documentList = new DocumentList(
                $this->getRouter(),
                $documentClass,
                $this->getDocumentTypeConfigStorage()->getTypeByClass($documentClass),
                $this->getDocumentRelationManager()->getDocumentRelation($documentClass, $documentRelation),
                $documentListRoutes
            );

            /* @var DocumentDataTable $dataTable */
            $documentDataTableOptions['documentList'] = $documentList;
            $dataTable = $this->getDataTableFactory()->build($documentDataTable ?? DocumentDataTable::class, $documentDataTableOptions);
            $documentList->setDataTable($dataTable);

            $documentList->setForm($this->getFormFactory()->create(
                DocumentListFormType::class,
                null,
                [
                    'action' => $documentList->getPostPath(),
                ]
            ));

            $this->handleRequest($documentList);

            return $documentList;
        } else {
            throw new DocumentListFactoryException("DocumentListFactory::create(): The class $documentClass should implement the DocumentInterface.");
        }
    }

    /**
     * @param DocumentListInterface $documentList
     *
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     * @throws DocumentListFactoryException
     */
    protected function handleRequest(DocumentListInterface $documentList)
    {
        if ($this->getRequestStack()->getCurrentRequest()->attributes->get('_route') == $documentList->getRoutes()['index']) {
            if ($documentList->getDocumentRelation()) {
                $entityRepository = $this->getObjectManager()->getRepository($documentList->getDocumentRelation()->getClass());
                $entity = $entityRepository->find($documentList->getDocumentRelation()->getIdentifier());
                $entityProperty = $documentList->getDocumentRelation()->getPropertyName();
                try {
                    $documents = $entity->{'get'.ucfirst($entityProperty)}();
                } catch (\Exception $e) {
                    throw new DocumentListFactoryException('DocumentListFactory::create(): The getter for entity property "'.$entityProperty.'" is missing.');
                }
            } else {
                $documents = $this->getObjectManager()->getRepository($documentList->getDocumentClass())->findAll();
            }

            $data = $this->getNormalizer()->normalize($documents, 'array');

            //TODO we should add pagination here if progressive scroll is ever used
            $documentList->getDataTable()->setData($data);
            $documentList->getDataTable()->setPerPage(40);
            $documentList->getDataTable()->setCount(\count($data));
        }
    }

    /**
     * @return ObjectManager
     */
    public function getObjectManager(): ObjectManager
    {
        return $this->objectManager;
    }

    /**
     * @param ObjectManager $objectManager
     *
     * @return DocumentListFactory
     */
    public function setObjectManager(ObjectManager $objectManager): DocumentListFactoryInterface
    {
        $this->objectManager = $objectManager;

        return $this;
    }

    /**
     * @return TranslatorInterface
     */
    public function getTranslator(): TranslatorInterface
    {
        return $this->translator;
    }

    /**
     * @param TranslatorInterface $translator
     *
     * @return DocumentListFactory
     */
    public function setTranslator(TranslatorInterface $translator): DocumentListFactoryInterface
    {
        $this->translator = $translator;

        return $this;
    }

    /**
     * @return Twig_Environment
     */
    public function getTwig(): Twig_Environment
    {
        return $this->twig;
    }

    /**
     * @param Twig_Environment $twig
     *
     * @return DocumentListFactory
     */
    public function setTwig(Twig_Environment $twig): self
    {
        $this->twig = $twig;

        return $this;
    }

    /**
     * @return RouterInterface|null
     */
    public function getRouter(): ?RouterInterface
    {
        return $this->router;
    }

    /**
     * @param RouterInterface|null $router
     *
     * @return DocumentListFactory
     */
    public function setRouter(?RouterInterface $router): self
    {
        $this->router = $router;

        return $this;
    }

    /**
     * @return TokenStorageInterface|null
     */
    public function getTokenStorage(): ?TokenStorageInterface
    {
        return $this->tokenStorage;
    }

    /**
     * @param TokenStorageInterface|null $tokenStorage
     *
     * @return DocumentListFactory
     */
    public function setTokenStorage(?TokenStorageInterface $tokenStorage): self
    {
        $this->tokenStorage = $tokenStorage;

        return $this;
    }

    /**
     * @return AuthorizationCheckerInterface|null
     */
    public function getAuthorizationChecker(): ?AuthorizationCheckerInterface
    {
        return $this->authorizationChecker;
    }

    /**
     * @param AuthorizationCheckerInterface|null $authorizationChecker
     *
     * @return DocumentListFactory
     */
    public function setAuthorizationChecker(?AuthorizationCheckerInterface $authorizationChecker): self
    {
        $this->authorizationChecker = $authorizationChecker;

        return $this;
    }

    /**
     * @return DataTableFactory
     */
    public function getDataTableFactory(): DataTableFactory
    {
        return $this->dataTableFactory;
    }

    /**
     * @param DataTableFactory $dataTableFactory
     *
     * @return DocumentListFactory
     */
    public function setDataTableFactory(DataTableFactory $dataTableFactory): self
    {
        $this->dataTableFactory = $dataTableFactory;

        return $this;
    }

    /**
     * @return DocumentTypeConfigStorage
     */
    public function getDocumentTypeConfigStorage(): DocumentTypeConfigStorage
    {
        return $this->documentTypeConfigStorage;
    }

    /**
     * @param DocumentTypeConfigStorage $documentTypeConfigStorage
     *
     * @return $this
     */
    public function setDocumentTypeConfigStorage(DocumentTypeConfigStorage $documentTypeConfigStorage): self
    {
        $this->documentTypeConfigStorage = $documentTypeConfigStorage;

        return $this;
    }

    /**
     * @return RequestStack
     */
    public function getRequestStack(): RequestStack
    {
        return $this->requestStack;
    }

    /**
     * @param RequestStack $requestStack
     *
     * @return $this
     */
    public function setRequestStack(RequestStack $requestStack): self
    {
        $this->requestStack = $requestStack;

        return $this;
    }

    /**
     * @return NormalizerInterface
     */
    public function getNormalizer(): NormalizerInterface
    {
        return $this->normalizer;
    }

    /**
     * @param NormalizerInterface $normalizer
     *
     * @return $this
     */
    public function setNormalizer(NormalizerInterface $normalizer): self
    {
        $this->normalizer = $normalizer;

        return $this;
    }

    /**
     * @return DocumentRelationManagerInterface
     */
    public function getDocumentRelationManager(): DocumentRelationManagerInterface
    {
        return $this->documentRelationManager;
    }

    /**
     * @param DocumentRelationManagerInterface $documentRelationManager
     *
     * @return $this
     */
    public function setDocumentRelationManager(DocumentRelationManagerInterface $documentRelationManager): self
    {
        $this->documentRelationManager = $documentRelationManager;

        return $this;
    }

    /**
     * @return FormFactoryInterface
     */
    public function getFormFactory(): FormFactoryInterface
    {
        return $this->formFactory;
    }

    /**
     * @param FormFactoryInterface $formFactory
     *
     * @return $this
     */
    public function setFormFactory(FormFactoryInterface $formFactory): self
    {
        $this->formFactory = $formFactory;

        return $this;
    }
}
