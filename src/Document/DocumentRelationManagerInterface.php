<?php

namespace DolmIT\DocumentsBundle\Document;

interface DocumentRelationManagerInterface
{
    /**
     * @param string $documentClass
     * @param null   $documentRelationIdentifier
     *
     * @return DocumentRelation|null
     */
    public function getDocumentRelation(string $documentClass, $documentRelationIdentifier = null): ?DocumentRelation;
}
