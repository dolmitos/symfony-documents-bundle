<?php

namespace DolmIT\DocumentsBundle\Document;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Inflector\Inflector;

class DocumentManager implements DocumentManagerInterface
{
    /**
     * @var DocumentListFactoryInterface
     */
    private $documentListFactory;

    /**
     * @var ObjectManager
     */
    private $objectManager;

    /**
     * @var DocumentRelationManagerInterface
     */
    private $documentRelationManager;

    /**
     * Supply a document class and a relation key to it's parent entity, if a relation should exist.
     *
     * @param string $documentClass
     * @param null   $documentRelation
     * @param null   $documentDataTable
     * @param array  $documentDataTableOptions
     *
     * @return DocumentListInterface
     */
    public function getDocumentList(
        string $documentClass,
        $documentRelation = null,
        $documentDataTable = null,
        $documentDataTableOptions = []
    ): DocumentListInterface {
        return $this->getDocumentListFactory()->create($documentClass, $documentRelation, $documentDataTable, $documentDataTableOptions);
    }

    /**
     * @param string        $documentClass
     * @param FormInterface $form
     * @param null          $documentRelation
     *
     * @return DocumentInterface|null
     *
     * @throws \Exception
     */
    public function saveDocument(string $documentClass, FormInterface $form, $documentRelation = null): ?DocumentInterface
    {
        if (!$form->getData() && !$form->getData()['file']) {
            return null;
        }

        $documentRelation = $this->getDocumentRelationManager()->getDocumentRelation($documentClass, $documentRelation);

        /** @var DocumentInterface $documentEntity */
        $documentEntity = new $documentClass();
        $documentEntity->setFile($form->getData()['file']);

        if ($documentRelation) {
            $documentRelationEntity = $this->getObjectManager()->find($documentRelation->getClass(), $documentRelation->getIdentifier());
            $documentRelationEntity->{'add'.ucfirst(Inflector::singularize($documentRelation->getPropertyName()))}($documentEntity);
            $this->getObjectManager()->persist($documentRelationEntity);
        } else {
            /*
             * TODO NB! When persisting the DocumentEntity directly, the file was not uploaded, but the relation was created properly.
             *      This should be tested as the need arises
            */
            $this->getObjectManager()->persist($documentEntity);
        }

        try {
            $this->getObjectManager()->flush();
        } catch (\Exception $e) {
            return null;
        }

        return $documentEntity;
    }

    /**
     * @param string $documentClass
     * @param string $documentIdentifier
     *
     * @return bool
     */
    public function deleteDocument(string $documentClass, string $documentIdentifier): bool
    {
        $document = $this->getObjectManager()->find($documentClass, $documentIdentifier);

        if ($document) {
            $this->getObjectManager()->remove($document);
            $this->getObjectManager()->flush();
        }

        return true;
    }

    /**
     * @param string $documentClass
     * @param string $documentIdentifier
     *
     * @return DocumentInterface|null
     */
    public function getDocument(string $documentClass, string $documentIdentifier): ?DocumentInterface
    {
        /** @var DocumentInterface|null $document */
        $document = $this->getObjectManager()->find($documentClass, $documentIdentifier);

        return $document ?? null;
    }

    /**
     * @return DocumentListFactoryInterface
     */
    public function getDocumentListFactory(): DocumentListFactoryInterface
    {
        return $this->documentListFactory;
    }

    /**
     * @param DocumentListFactoryInterface $documentListFactory
     *
     * @return $this
     */
    public function setDocumentListFactory(DocumentListFactoryInterface $documentListFactory): self
    {
        $this->documentListFactory = $documentListFactory;

        return $this;
    }

    /**
     * @return ObjectManager
     */
    public function getObjectManager(): ObjectManager
    {
        return $this->objectManager;
    }

    /**
     * @param ObjectManager $objectManager
     *
     * @return $this
     */
    public function setObjectManager(ObjectManager $objectManager): self
    {
        $this->objectManager = $objectManager;

        return $this;
    }

    /**
     * @return DocumentRelationManagerInterface
     */
    public function getDocumentRelationManager(): DocumentRelationManagerInterface
    {
        return $this->documentRelationManager;
    }

    /**
     * @param DocumentRelationManagerInterface $documentRelationManager
     *
     * @return $this
     */
    public function setDocumentRelationManager(DocumentRelationManagerInterface $documentRelationManager): self
    {
        $this->documentRelationManager = $documentRelationManager;

        return $this;
    }
}
