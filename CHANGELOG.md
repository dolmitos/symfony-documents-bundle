# Changelog
All notable changes to this project will be documented in this file.

## [Unreleased]

## [0.2.1] - 2019-09-25
- Redraw tabulator after files are added to the documents list by manual upload
               
  This is to fix the issue with some browsers where a horizontal scroll appears


## [0.2] - 2019-09-05
- Add endpoint for document download
- Implement separate view form for documents and add href field for fileData.originalName
- Fix this scope in formatter override
- Update to Datatables bundle 0.5.6
- Add parent class to datatable containing div to fix potential issues

## [0.1.1] - 2019-08-14
- Fix issue where upcoming queue files were removed before they were uploaded
- Fix issue where remove icon is missing due to the row being in edit state by default

## [0.1] - 2019-08-14
- Release basic Documents bundle

[Unreleased]: https://gitlab.com/dolmitos/symfony-documents-bundle/compare/v0.2.1...HEAD
[0.2.1]: https://gitlab.com/dolmitos/symfony-documents-bundle/compare/v0.2...v0.2.1
[0.2]: https://gitlab.com/dolmitos/symfony-documents-bundle/compare/v0.1.1...v0.2
[0.1.1]: https://gitlab.com/dolmitos/symfony-documents-bundle/compare/v0.1...v0.1.1
[0.1]: https://gitlab.com/dolmitos/symfony-documents-bundle/compare/v0.1
